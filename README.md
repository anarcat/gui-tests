Hello world in various Python GUI toolkits
==========================================

This is a bunch of simple GUI test programs to compare different
frameworks to replace PyGTK2 for two programs I wrote about 10 years
ago. PyGTK2 is now deprecated and is being removed from various
distributions, which forces me to find a new toolkit.

PyGTK3 (or rather, gi) is not listed because i am upset that they
abandoned gtk2 and that I need to rewrite everything like this.

Reviewed projects include:

 * [Kivy](https://kivy.org/): crashes in Py3, works on Mac, Windows, Linux, Android,
   iOS
 * [Pygame](https://pygame.org/): Mac, Windows, Linux, Android
 * [PySDL2](https://pysdl2.readthedocs.io): very low-level, should work wherever [SDL](http://www.libsdl.org/) does:
   Mac, Windows, Linux, Android
 * [PyQT4](https://pypi.python.org/pypi/PyQt4), [PyQT5](https://pypi.python.org/pypi/PyQt5): Mac, Windows, Linux... Android?
 * [ŴxPython](https://www.wxpython.org/): not ported to Py3, works in Mac, Windows, Linux
