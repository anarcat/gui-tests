#!/usr/bin/python

# hellow world with Kivy
#
# taken from:
# https://kivy.org/docs/guide/basic.html#create-an-application
#
# fails under Python3 in Debian stretch:
# x11 - ImportError: No module named 'kivy.core.window.window_x11'

from kivy.app import App
from kivy.uix.label import Label


class MyApp(App):

    def build(self):
        return Label(text='Hello world')


if __name__ == '__main__':
    MyApp().run()
