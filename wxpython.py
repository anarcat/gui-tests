#!/usr/bin/python
#
# hellow world with wxwidgets, copy of:
# https://www.wxpython.org/pages/overview/#hello-world
#
# wxpython not ported to py3 yet.

# First things, first. Import the wxPython package.
import wx

# Next, create an application object.
app = wx.App()

# Then a frame.
frm = wx.Frame(None, title="Hello World")

# Show it.
frm.Show()

# Start the event loop.
app.MainLoop()
