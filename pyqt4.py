#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# modified copy of:
# https://fr.wikibooks.org/wiki/PyQt/Premier_exemple_:_Hello_World_!

from PyQt4 import QtGui
import sys
 
app = QtGui.QApplication(sys.argv)
hello = QtGui.QPushButton("Hello World!", None)
hello.show()
app.exec_()
