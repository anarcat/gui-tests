#!/usr/bin/python3

# basic hellow world in SDL
#
# based on:
# https://pysdl2.readthedocs.io/en/rel_0_9_6/tutorial/helloworld.html
#
# note that this doesn't actually show the "hello world" string but a
# bitmap

import sdl2.ext

sdl2.ext.init()

window = sdl2.ext.Window("Hello World!", size=(640, 480))
window.show()

factory = sdl2.ext.SpriteFactory(sdl2.ext.SOFTWARE)
sprite = factory.from_image('/usr/share/pixmaps/debian-logo.png')

spriterenderer = factory.create_sprite_render_system(window)
spriterenderer.render(sprite)
processor = sdl2.ext.TestEventProcessor()
processor.run(window)
sdl2.ext.quit()
